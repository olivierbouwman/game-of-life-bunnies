Game of Life type simulation for bunnies

IDEAS
* show total live male and female bunnies, generations, deaths by age, deaths by energy, etc.
* randomize (gaussian) age, mating success
* make bunnyMaxEnergy dependent on age so babies and seniors die quicker
* make chance of hopping dependant on energy/age so older and tired bunnies hop less
* automatic reset if no bunnies are left
* predators / virus / disease